# Longform Markdown Splitter Obsidian-Plugin

A fancy user interface for the longform-markdown-splitter in the form of an Obsidian plugin. Allows you to push Obsidian content into a Hugo project (or other website builders).

Build for Obsidian with [Typescript](https://www.typescriptlang.org/) and [Svelte](https://svelte.dev)

- Under Development -
