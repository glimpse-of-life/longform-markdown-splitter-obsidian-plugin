import { Editor, type MarkdownFileInfo, MarkdownView } from "obsidian";

const PTE_ObsidianSampleCommand = {
	id: "sample-editor-command",
	name: "Sample Editor-Command",
	editorCallback: (
		editor: Editor,
		view: MarkdownView | MarkdownFileInfo
	) => {
		console.log(editor.getSelection());
		editor.replaceSelection("Sample Editor Command");
	},
}

export default PTE_ObsidianSampleCommand;
