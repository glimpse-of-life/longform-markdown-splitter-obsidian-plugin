import type { Moment } from "moment";
import type { TFile } from "obsidian";

// import { createDailyNote, getDailyNoteSettings, } from "obsidian-daily-notes-interface";

import { PTE_ObsidianConfirmationModal } from "../PTE_ObsidianConfirmationModal";
// import type { ISettings } from "src/settings";
import { type PTE_Settings } from "PTE_Settings";

/**
 * Create a Daily Note for a given date.
 */
export async function tryToCreateDailyNote(
	date: Moment,
	inNewSplit: boolean,
	settings: PTE_Settings,
	cb?: (newFile: TFile) => void
): Promise<void> {

/*
  const { workspace } = window.app;
  const { format } = getDailyNoteSettings();
  const filename = date.format(format);
*/
	const filename = "todo-test.md";

	const createFile = async () => {
		// const dailyNote = await createDailyNote(date);
		// const leaf = inNewSplit ? workspace.splitActiveLeaf(): workspace.getUnpinnedLeaf();
		// await leaf.openFile(dailyNote, { active : true });
		// cb?.(dailyNote);
	};

	// TODO app deprecated
	if (settings.confirmNoteCreation) {
		PTE_ObsidianConfirmationModal.createAndOpen(app, {
			title: "New Daily Note",
			text: `File ${filename} does not exist. Would you like to create it?`,
			confirm_button_text: "Create",
			onConfirm: createFile,
		});
	} else {
		await createFile();
	}
}
