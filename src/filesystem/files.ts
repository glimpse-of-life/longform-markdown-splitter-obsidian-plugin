import {
	normalizePath,
	Notice,
	TFile,
	TFolder,
	Vault,
	Workspace,
} from "obsidian";

export async function openFile(file: TFile, workspace: Workspace) {
	const leaf = workspace.getLeaf();
	await leaf.openFile(file, { active: true });
}

export function findFiles(notesFolder: TFolder) {
	const noteFiles: TFile[] = [];

	Vault.recurseChildren(notesFolder, async (note) => {
		if (note instanceof TFile) {
			noteFiles.push(note);
		}
	});
	return noteFiles;
}
