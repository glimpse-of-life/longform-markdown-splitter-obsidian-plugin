import { App, Modal } from "obsidian";

export interface PTE_ObsidianConfirmationModal_Parameter {
	title: string;
	text: string;
	confirm_button_text: string;

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	onConfirm: (...args: any[]) => Promise<void>;
}

export class PTE_ObsidianConfirmationModal extends Modal {
	/**
	 * Creates an instance of PTE_ObsidianConfirmationModal.
	 * 
	 * @param {App} app
	 * @param {PTE_ObsidianConfirmationModal_Parameter} cfg
	 * @memberof PTE_ObsidianConfirmationModal
	 */
	constructor(app: App, cfg: PTE_ObsidianConfirmationModal_Parameter) {
		super(app);

		const { confirm_button_text: confirm_button_text, onConfirm: onConfirm, text, title } = cfg;

		this.contentEl.createEl("h2", { text: title });
		this.contentEl.createEl("p", { text });

		this.contentEl.createDiv("modal-button-container", (buttonsEl) => {
			buttonsEl
				.createEl("button", { text: "No" })
				.addEventListener("click", () => this.close());

			buttonsEl
				.createEl("button", {
					cls: "mod-cta",
					text: confirm_button_text,
				})
				.addEventListener("click", async (e) => {
					await onConfirm(e);
					this.close();
				});
		});
	}

	onOpen(): void {
		console.log("PTE_ObsidianConfirmationModal opened");
	}

	onClose(): void {
		const { contentEl } = this;
		contentEl.empty();
		console.log("PTE_ObsidianConfirmationModal closed");
	}

	/**
	 * This Factory creates and opens an Instance of the Class.
	 *
	 * @static
	 * @param {App} app
	 * @param {PTE_ObsidianConfirmationModal_Parameter} { confirm_button_text: cta, onAccept, text, title }
	 * @return {*}  {PTE_ObsidianConfirmationModal}
	 * @memberof PTE_ObsidianConfirmationModal
	 */
	static createAndOpen(
		app: App,
		{ confirm_button_text: confirm_button_text, onConfirm: onConfirm, text, title }: PTE_ObsidianConfirmationModal_Parameter
	): PTE_ObsidianConfirmationModal {

		const modal = new PTE_ObsidianConfirmationModal(app, {
			confirm_button_text,
			onConfirm,
			text,
			title,
		});

		modal.open();

		return modal;
	}
}
