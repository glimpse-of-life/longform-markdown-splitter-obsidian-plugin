import {
	Notice,
	Plugin,
	Platform,
	WorkspaceLeaf,
	MarkdownView,
} from "obsidian";

import PTE_ObsidianPluginSettingsTab from "./PTE_ObsidianPluginSettingTab";

import { type PTE_Settings, DEFAULT_SETTINGS } from "./PTE_Settings";

import { PTE_ObsidianMainView, VIEW_TYPE_MAIN_VIEW } from "./PTE_ObsidianMainView";
import { PTE_ObsidianConfirmationModal } from "PTE_ObsidianConfirmationModal";
import { SampleModal } from "SampleModal";
// import PTE_ObsidianSampleCommand from "commands/PTE_ObsidianSampleCommand";

export default class PTE_ObsidianPlugin extends Plugin {
	
	settings: PTE_Settings = DEFAULT_SETTINGS;
	pluginLongName = "Publish-To-External Plugin";


	doSomeThing = async () => {
		console.log("PTE_ObsidianPlugin -> PTE_ObsidianSampleCommand -> async doSomeThing");
	};

	async onload() {
		console.log(`loading ${this.pluginLongName}`);

		this.registerView(
			VIEW_TYPE_MAIN_VIEW,
			(leaf: WorkspaceLeaf) => new PTE_ObsidianMainView(leaf, this)
		);

		if (Platform.isIosApp) {
			// ...
		} else if (Platform.isAndroidApp) {
			// ...
		} else {
			// desktop
		}

		await this.loadSettings();

		// This creates an icon in the left ribbon.
		// https://lucide.dev
		// Only icons up to v0.171.0 are supported at this time.
		const ribbonIconEl = this.addRibbonIcon(
			"book",
			"Publish To External Plugin",
			(evt: MouseEvent) => {
				//* Called when the user clicks the icon.
				new Notice("This is a Memories-Diary notice!");

				this.activateListView();

				// TODO Template Property: diaryEntryScope: day, weeek, month, quartal, jahr, dekade.
			}
		); // addRibbonIcon

		// Perform additional things with the ribbon
		ribbonIconEl.addClass("pte-plugin-ribbon-class");

		//* This adds a status bar item to the bottom of the app. Does not work on mobile apps.
		const statusBarItemEl = this.addStatusBarItem();
		statusBarItemEl.setText(`Hello from ${this.pluginLongName}`);

		//* This adds a simple command that can be triggered anywhere
		// Command Palette
		this.addCommand({
			id: "pte-modal-simple",
			name: "Open Modal (simple)",
			callback: () => {
				PTE_ObsidianConfirmationModal.createAndOpen(this.app, {
					title: "My Command (simple) Callback",
					text: `Hi from Command (simple)...`,
					confirm_button_text: "Create",
					onConfirm: this.doSomeThing,
				});
			},
		}); // addCommand

		//* This adds an editor command that can perform some operation on the current editor instance
/*
		this.addCommand({
			id: 'sample-editor-command',
			name: 'Sample editor command',
			editorCallback: (editor: Editor, view: MarkdownView) => {
				console.log(editor.getSelection());
				editor.replaceSelection('Sample Editor Command');
			}
		}); // addCommand
*/
		//* This adds a complex command that can check whether the current state of the app allows execution of the command
		this.addCommand({
			id: 'open-sample-modal-complex',
			name: 'Open sample modal (complex)',
			checkCallback: (checking: boolean) => {
				// Conditions to check
				const markdownView = this.app.workspace.getActiveViewOfType(MarkdownView);
				if (markdownView) {
					// If checking is true, we're simply "checking" if the command can be run.
					// If checking is false, then we want to actually perform the operation.
					if (!checking) {
						new SampleModal(this.app).open();
					}

					// This command will only show up in Command Palette when the check function returns true
					return true;
				}
			}
		}); // addCommand

		//* This adds a settings tab so the user can configure various aspects of the plugin
		this.addSettingTab(new PTE_ObsidianPluginSettingsTab(this.app, this));

		// If the plugin hooks up any global DOM events (on parts of the app that doesn't belong to this plugin)
		// Using this function will automatically remove the event listener when this plugin is disabled.
		this.registerDomEvent(document, "click", (evt: MouseEvent) => {
			console.log("click", evt);
		});

		// When registering intervals, this function will automatically clear the interval when the plugin is disabled.
		this.registerInterval(
			window.setInterval(() => console.log("setInterval"), 5 * 60 * 1000)
		);
	} // onLoad

	async activateListView() {
		const { workspace } = this.app;

		let leaf: WorkspaceLeaf | null = null;
		const leaves = workspace.getLeavesOfType(VIEW_TYPE_MAIN_VIEW);

		if (leaves.length > 0) {
			// A leaf with our view already exists, use that
			leaf = leaves[0];
		} else {
			// Our view could not be found in the workspace, create a new leaf
			// in the right sidebar for it
			const leaf = workspace.getLeftLeaf(false);
			await leaf.setViewState({ type: VIEW_TYPE_MAIN_VIEW, active: true });
		}

		// "Reveal" the leaf in case it is in a collapsed sidebar
		if (leaf != null) workspace.revealLeaf(leaf);
	} // activateListView

	onunload() {
		console.log(`unloading ${this.pluginLongName}`);
		const { workspace } = this.app;
		workspace.detachLeavesOfType(VIEW_TYPE_MAIN_VIEW);
	} // onUnload

	async loadSettings() {
		this.settings = Object.assign(
			{},
			DEFAULT_SETTINGS,
			await this.loadData()
		);
	}

	async saveSettings() {
		await this.saveData(this.settings);
	}
}
