import { App, PluginSettingTab, Setting } from "obsidian";
import PTE_ObsidianPlugin from "main";

interface Props {
	name: string;
	desc: string;
	ph: string;
	settingValue: string;
	onChange: (value: string) => void;
}

export default class PTE_ObsidianPluginSettingsTab extends PluginSettingTab {
	
	plugin: PTE_ObsidianPlugin;

	constructor(app: App, plugin: PTE_ObsidianPlugin) {
		super(app, plugin);
		this.plugin = plugin;
	}

	getSetting(containerEl: HTMLElement, props: Props): Setting {
		return new Setting(containerEl)
			.setName(props.name)
			.setDesc(props.desc)
			.addText((text) =>
				text
					.setPlaceholder(props.ph)
					.setValue(props.settingValue)
					.onChange(async (value) => {
						await props.onChange(value);
					})
			);
	}

	display(): void {
		const { containerEl } = this;

		containerEl.empty();

		// TODO Datum statt Text
		new Setting(containerEl)
			.setName("Geburtstag")
			.setDesc("Wann wurdest du geboren?")
			.addText((text) =>
				text
					.setPlaceholder("Enter your Birthday")
					.setValue(this.plugin.settings.birthday)
					.onChange(async (value) => {
						this.plugin.settings.birthday = value;
						await this.plugin.saveSettings();
					})
			);

		// TODO Number statt Text
		new Setting(containerEl)
			.setName("Lebensspanne")
			.setDesc("geschätzte Lebensdauer")
			.addText((text) =>
				text
					.setPlaceholder("100")
					.setValue(this.plugin.settings.lifespan.toString())
					.onChange(async (value) => {
						this.plugin.settings.lifespan = Number(value);
						await this.plugin.saveSettings();
					})
			);

		this.getSetting(containerEl, {
			name: "Pfad zum Tagebuch",
			desc: "Der Pfad zur Ablage der Tagebucheinträge.",
			ph: "Enter the Path",
			settingValue: this.plugin.settings.day_folder,
			onChange: async function (value: string): Promise<void> {

				// TODO: this.plugin.settings.day_folder = value;
				// TODO: await this.plugin.saveSettings();
				
			},
		});

		new Setting(containerEl)
			.setName("")
			.setDesc("")
			.addText((text) =>
				text
					.setPlaceholder("")
					.setValue(this.plugin.settings.day_folder)
					.onChange(async (value) => {
						this.plugin.settings.day_folder = value;
						await this.plugin.saveSettings();
					})
			);

		new Setting(containerEl)
			.setName("Pfad zum Template")
			.setDesc("Der Pfad zum Template für einen Tagebucheintrag.")
			.addText((text) =>
				text
					.setPlaceholder("Enter the Path")
					.setValue(this.plugin.settings.day_template)
					.onChange(async (value) => {
						this.plugin.settings.day_template = value;
						await this.plugin.saveSettings();
					})
			);
	} // display
}
