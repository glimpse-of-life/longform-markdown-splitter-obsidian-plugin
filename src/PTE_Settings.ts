interface PTE_Settings {
	birthday: string;
	lifespan: number;

	day_enabled: boolean;
	day_folder: string;
	day_format: string;
	day_template: string;

	week_enabled: boolean;
	week_folder: string;
	week_format: string;
	week_template: string;

	quartal_enabled: boolean;
	quartal_folder: string;
	quartal_format: string;
	quartal_template: string;

	year_enabled: boolean;
	year_folder: string;
	year_format: string;
	year_template: string;

	decade_enabled: boolean;
	decade_folder: string;
	decade_format: string;
	decade_template: string;

	confirmNoteCreation: boolean;
}

const DEFAULT_SETTINGS: PTE_Settings = {
	birthday: "1963-08-17 00:00",
	lifespan: 100,

	day_enabled: true,
	day_folder: "Tagebuch/",
	day_format: "[Tagebuch; ] YYYY-MM-DD dddd",
	day_template: "templates/Tagebuch-Day-Template.md",

	week_enabled: true,
	week_folder: "Tagebuch/",
	week_format: "[Tagebuch; ] YYYY-MM",
	week_template: "templates/Tagebuch-Week-Template.md",

	quartal_enabled: true,
	quartal_folder: "Tagebuch/",
	quartal_format: "[Tagebuch; ] YYYY-MM",
	quartal_template: "templates/Tagebuch-Week-Template.md",

	year_enabled: true,
	year_folder: "Tagebuch/",
	year_format: "[Tagebuch; ] YYYY-MM",
	year_template: "templates/Tagebuch-Year-Template.md",

	decade_enabled: true,
	decade_folder: "Tagebuch/",
	decade_format: "[Tagebuch; ] YYYY-MM",
	decade_template: "templates/Tagebuch-Decade-Template.md",

	confirmNoteCreation: true,
};

export { DEFAULT_SETTINGS };
export type { PTE_Settings };
