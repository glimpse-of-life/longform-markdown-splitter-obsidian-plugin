import { ItemView, TFile, TFolder, WorkspaceLeaf, normalizePath } from "obsidian";

import PTE_ObsidianPlugin from "main";
import MainView_Svelte from "./svelte/MainView.svelte";
import { findFiles } from "filesystem/files";

export const VIEW_TYPE_MAIN_VIEW = "pte-main-view";

export class PTE_ObsidianMainView extends ItemView {
	plugin: PTE_ObsidianPlugin;

	mainView: MainView_Svelte | null = null;

	constructor(leaf: WorkspaceLeaf, plugin: PTE_ObsidianPlugin) {
		super(leaf);
		this.plugin = plugin;

		// TODO: test - lädt alle Dateien
		const contactsFolder = plugin.app.vault.getAbstractFileByPath( normalizePath('Tagebuch')) as TFolder;
		const diaryFiles: TFile[] = findFiles(contactsFolder);
		console.log('Tagebuch Dateien', diaryFiles)
	}

	getViewType() {
		return VIEW_TYPE_MAIN_VIEW;
	}

	getDisplayText() {
		return "All My Memories";
	}

	getIcon(): string {
		return "book-open"; // TODO Spezielles Icon für die Liste
	}

	async onOpen() {
		this.mainView = new MainView_Svelte({
			target: this.contentEl,
			props: {
				plugin: this.plugin,
			},
		});
	}

	async onClose() {
		this.mainView?.$destroy();
	}
}
